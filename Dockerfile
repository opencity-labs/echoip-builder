FROM alpine/httpie as build

ARG LICENSE_KEY

WORKDIR /var/tmp

# extract with --strip-components because path inside tar containes date version
# result:
#    GeoLite2-ASN_20230131/GeoLite2-ASN.mmdb -> GeoLite2-ASN.mmdb
RUN http -vv --download -o /tmp/geoip.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=${LICENSE_KEY}&suffix=tar.gz"
RUN    ls -l $(pwd) && \
    tar xzvf geoip.tar.gz "*.mmdb" --strip-components 1

# SHA256 URL
# https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=${LICENSE_KEY}&suffix=tar.gz.sha256

# Database URL
RUN http -vv --download -o GeoLite2-ASN-CSV.ziphttps://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN-CSV&license_key=${LICENSE_KEY}&suffix=zip
# SHA256 URL
# https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN-CSV&license_key=${LICENSE_KEY}&suffix=zip.sha256

# Database URL
RUN -vv --download -o GeoLite2-City.tar.gz https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=${LICENSE_KEY}&suffix=tar.gz
# SHA256 URL
#https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=${LICENSE_KEY}&suffix=tar.gz.sha256

FROM mpolden/echoip

COPY --from=build /tmp/* /tmp/

CMD [ "-H", "X-Real-IP", "-a", "/tmp/asn.db", "-c", "city.db", "-f", "country.db", "-r" ]

